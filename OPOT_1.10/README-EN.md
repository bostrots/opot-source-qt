## OnePlus-Toolkit version 1.10

## Installation
This toolkit don't have to be installed and can be started just by a click on OPOT.exe.

## Usage
When you start the toolkit you only have to press the button for the function you want to use and
the toolkit will do the rest. Note that you have to installed ADB for that.

## TODO:
OneClick Root/App
Update/Backup functions
CyanogenMod Nightly Updater
Help, feedback and other update functions
more languages
++and more. Please write me what you like to see

## Contributing
1. Fork on GitHub
2. Create your own branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push it to your branch: `git push origin my-new-feature`
5. Request a PUSH update :D

## Chronik
OPOT_1.01 - New features
OPOT_1.02 - New features
OPOT_1.03 - New features
OPOT_1.04 - New features
OPOT_1.05 - New features
OPOT_1.06 - New features
OPOT_1.07 - New features
OPOT_1.08 - New features
OPOT_1.09 - New features
OPOT_1.10 - New features

## Credits
Bostrot - Programmed
rarog - Optimized

## License
in License.txt