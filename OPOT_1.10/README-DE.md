## OnePlus-Toolkit version 1.10

## Installation
Das Toolkit muss nicht installiert werden und kann mit einem Klick auf OPOT.exe gestartet werden.

## Usage
Wenn das Toolkit gestartet wurde, muss lediglich der Button f�r die entsprechende Funktion gedr�ckt
werden und schon wird Ihr Android Smartphone diese Funktion ausf�hren. Daf�r werden die ADB Treiber
ben�tigt. Weitere Informationen im Forum.

## TODO:
OneClick Root/App
Update/Sicherungsfunktionen
CyanogenMod Nightly Updater
Hilfe, Feedback und andere Update Funktionen
Mehr Sprachen
++und mehr, gerne auch Vorschl�ge

## Contributing
1. Auf GitHub forken
2. Eigene branch erstellen: `git checkout -b my-new-feature`
3. Kommite deine �nderungen: `git commit -am 'Add some feature'`
4. Pushe es zur branch: `git push origin my-new-feature`
5. Ver�ffentliche eine Pust Anfrage :D

## Chronik
OPOT_1.01 - New features
OPOT_1.02 - New features
OPOT_1.03 - New features
OPOT_1.04 - New features
OPOT_1.05 - New features
OPOT_1.06 - New features
OPOT_1.07 - New features
OPOT_1.08 - New features
OPOT_1.09 - New features
OPOT_1.10 - New features

## Credits
Bostrot - Programmieren des Programms
rarog - Verbesserungen

## License
in License.txt